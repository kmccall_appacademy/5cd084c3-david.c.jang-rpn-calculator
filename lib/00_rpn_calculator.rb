class RPNCalculator
  def initialize
    @stack = []

  end

  def push(val)
    @stack << val
  end

  def times
    operation_failcheck
    value = @stack.pop * @stack.pop
    @stack << value
  end

  def plus
    operation_failcheck
    value = @stack.pop + @stack.pop
    @stack << value
  end

  def minus
    operation_failcheck
    value = -(@stack.pop) +  @stack.pop
    @stack << value
  end

  def divide
    operation_failcheck
    value = 1.fdiv(@stack.pop) * @stack.pop
    @stack << value
  end

  def value
    @stack.last
  end

  def tokens(string)
    string.split.map do |token|
      "*/+-".include?(token) ? token.to_sym : token.to_i
    end
  end

  def evaluate(string)
    token_stack = tokens(string)
    token_stack.each do |token|
      case token
      when Integer
        push(token)
      when Symbol
        operations(token)
      end
    end
    value

  end

  def operations(symbol)
    case symbol

    when :+
      self.plus
    when :*
      self.times
    when :/
      self.divide
    when :-
      self.minus
    end
  end

  private

  def operation_failcheck
    raise "calculator is empty" if @stack.length < 2
  end



end
